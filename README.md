# Python 114


### Assignments

- [How to Submit Assignments](https://www.youtube.com/watch?v=jXpT8eOzzCM)
- [Assignment 1](https://classroom.github.com/a/Shzd_n_K)
- [Assignment 2](https://classroom.github.com/a/U6SHeAn0)
- [Assignment 3](https://classroom.github.com/a/t06RXiR4)
- [Assignment 4](https://classroom.github.com/a/5WNKOPX5)
- [Assignment 5](https://classroom.github.com/a/1-q9uSkj)
- [Assignment 6](https://classroom.github.com/a/zo27MHjD)
- [Assignment 7](https://classroom.github.com/a/VJK4XsgO)
- [Assignment 8](https://classroom.github.com/a/e-RY2uhP)

### Resources

- [Python Book](https://1drv.ms/b/s!AmZJMrBsKhiOhYRVjF_6FufcwBQI8w)


### Docs

- [QuerySet](./docs/QUERYSET.md)
- [Template Language](./docs/TEMPLATE_LANGUAGE.md)
- [Django Forms](./docs/DJANGO_FORMS.md)
- [Generic Views](./docs/GENERIC_VIEWS.md)

[Final Project](./python_114.pdf)