print('Hello\nWorld') # \n new line character
print('Hi there \'luka\'')
print('Hi there "Josh"')
print("Hi there 'Josh'")
print("Hi there \"Josh\"")

print('This\tword\tis\ttab\tspaced')

text = "      rwe;kguhewrotuwhe m       "
print(text)
print(text.strip())
print(text.rstrip())
print(text.lstrip())
