# type casting
age = 25

print(type(age))
age = str(age)
print('You are ' + age + ' years old')
print(int('2500'))
age = int(age)
age = float(age)
print(type(age))
print(str(0.1 + 0.2))
print(0.1 + 0.2)

print(f'{bool(1000) = }')
print(f'{bool(-1000) = }')
print(f'{bool(1.9) = }')
print(f'{bool(0) = }')
print(f'{bool(0.9) = }')


print(f'{bool("Hello") = }')
print(f'{bool("-25") = }')
print(f'{bool("") = }')
print(f'{bool(" ") = }')






