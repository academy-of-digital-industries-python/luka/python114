# Django Queryset API


### What is API?
API stands for Application Programming Interface. It is a set of rules that allows one piece of software application to talk to another. It defines the methods for communication between the software components.


### What is QuerySet?
A QuerySet is a collection of database queries to retrieve data from the database. It can be used to filter the results based on the given parameters. It is a powerful tool to interact with the database.

we can use QuerySet to perform the following operations:
- Retrieve data from the database
- Filter data based on the given parameters
- Order the data
- Create new data
- Update the data
- Delete the data
- Perform complex queries


### How to create a QuerySet?
To create a QuerySet, we can use the `objects` attribute of the model class. The `objects` attribute is a manager class that provides a QuerySet API to interact with the database.

```python
from myapp.models import MyModel

# Get all the objects of the model
queryset = MyModel.objects.all()

# Filter the objects based on the given parameters
queryset = MyModel.objects.filter(field1=value1, field2=value2)

# Order the objects based on the given field
queryset = MyModel.objects.order_by('field1')

# Create a new object
obj = MyModel.objects.create(field1=value1, field2=value2)

# Update the object
obj.field1 = new_value
obj.save()

# Delete the object
obj.delete()

# Perform complex queries
queryset = MyModel.objects.filter(field1=value1).exclude(field2=value2)

```

### How to retrieve data from the QuerySet?
To retrieve data from the QuerySet, we can use the following methods:
- `all()`: It returns all the objects of the model.
- `filter()`: It filters the objects based on the given parameters.
- `exclude()`: It excludes the objects based on the given parameters.
- `get()`: It returns a single object based on the given parameters.
- `first()`: It returns the first object of the QuerySet.
- `last()`: It returns the last object of the QuerySet.
- `count()`: It returns the number of objects in the QuerySet.
- `exists()`: It returns True if the QuerySet has any objects, otherwise False.

```python
from myapp.models import MyModel

# Get all the objects of the model
queryset = MyModel.objects.all()

# Filter the objects based on the given parameters
queryset = MyModel.objects.filter(field1=value1, field2=value2)

# Exclude the objects based on the given parameters
queryset = MyModel.objects.exclude(field1=value1)

# Get a single object based on the given parameters
obj = MyModel.objects.get(field1=value1)

# Get the first object of the QuerySet
obj = MyModel.objects.first()

# Get the last object of the QuerySet
obj = MyModel.objects.last()

# Get the number of objects in the QuerySet
count = MyModel.objects.count()

# Check if the QuerySet has any objects
exists = MyModel.objects.filter(field1=value1).exists()

```

### How to build query using `Q` objects?
The `Q` object allows us to build complex queries using logical operators like `AND`, `OR`, and `NOT`. We can combine multiple `Q` objects to create complex queries.

- `AND` operation: We can use the `&` operator to combine multiple `Q` objects with the `AND` operation.
- `OR` operation: We can use the `|` operator to combine multiple `Q` objects with the `OR` operation.
- `NOT` operation: We can use the `~` operator to negate a `Q` object.

```python
from django.db.models import Q
from myapp.models import MyModel

# Build a query using Q objects
queryset = MyModel.objects.filter(Q(field1=value1) | Q(field2=value2))

# Combine multiple Q objects
queryset = MyModel.objects.filter(Q(field1=value1) & ~Q(field2=value2))
```

### CRUD Operations
CRUD stands for Create, Read, Update, and Delete. We can perform CRUD operations using the QuerySet API.

- Create: We can create a new object using the `create()` method.
- Read: We can retrieve data from the database using the `all()`, `filter()`, `get()`, `first()`, `last()`, `count()`, and `exists()` methods.
- Update: We can update an object using the `save()` method.
- Delete: We can delete an object using the `delete()` method.
