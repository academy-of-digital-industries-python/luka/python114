# Django Generic Views

## What are Generic Views?

Generic views are pre-built views provided by Django that can be used to perform common tasks such as displaying data, creating new objects, updating objects, and deleting objects. They are designed to reduce the amount of code required to perform these tasks and to provide a consistent interface for interacting with the database.

Generic views are implemented as classes that inherit from Django's `View` class. They provide methods for handling different HTTP methods such as `GET`, `POST`, `PUT`, and `DELETE`. By using generic views, we can quickly create views for common tasks without having to write the view logic from scratch.

## Types of Generic Views
- `ListView`: Displays a list of objects.
- `DetailView`: Displays details of a single object.
- `CreateView` Creates a new object.
- `UpdateView`: Updates an existing object.
- `DeleteView`: Deletes an existing object.

## Using Generic Views
To use generic views in a Django project, we need to define a URL pattern that maps a URL to a generic view class. We can specify the model to use for the view and other parameters such as the template name and context data.

```python
from django.urls import path
from .views import MyListView

urlpatterns = [
    path('my-list/', MyListView.as_view(), name='my-list'),
]
```

In the above example, we have defined a URL pattern that maps the URL `/my-list/` to the `MyListView` class. The `MyListView` class is a generic view that displays a list of objects. We can specify additional parameters such as the model to use and the template name in the view class.

## Customizing Generic Views
Generic views can be customized by overriding the methods provided by the base class. We can customize the behavior of the view by overriding methods such as `get_queryset`, `get_context_data`, `form_valid`, and `form_invalid`.

```python
from django.views.generic import ListView
from .models import MyModel

class MyListView(ListView):
    model = MyModel
    template_name = 'my_list.html'
    context_object_name = 'my_objects'

    def get_queryset(self):
        return MyModel.objects.filter(is_active=True)
```

In the above example, we have customized the `MyListView` class by overriding the `get_queryset` method. This method returns a filtered queryset of objects based on a custom condition. We can customize other methods to modify the behavior of the view as needed.

## Using Mixins with Generic Views
Mixins are classes that provide additional functionality to generic views. We can use mixins to add common functionality to multiple views without duplicating code. Mixins can be combined with generic views to create custom views with specific behavior.

```python
from django.views.generic import ListView
from .models import MyModel

class MyMixin:
    def get_queryset(self):
        return MyModel.objects.filter(is_active=True)
        
class MyListView(MyMixin, ListView):
    model = MyModel
    template_name = 'my_list.html'
    context_object_name = 'my_objects'
```

In the above example, we have defined a mixin class `MyMixin` that provides a custom `get_queryset` method. We have then combined the mixin with the `ListView` class to create a custom view `MyListView` that displays a list of objects filtered by a custom condition.