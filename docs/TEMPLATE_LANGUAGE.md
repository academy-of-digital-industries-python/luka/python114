# Django Template Language

Django Template Language is a powerful tool to generate dynamic HTML content. It allows us to embed Python code within HTML templates to create dynamic web pages. In this guide, we will learn about the Django Template Language and how to use it to create dynamic web pages.

## Accessing Variables

We can access variables in the Django Template Language by using the double curly braces `{{ variable }}`. The variable can be a context variable passed from the view or a template variable defined in the template.

```html
<p>
    Hello, {{ name }}!
</p>
```

In the above example, the `name` variable will be replaced with the value of the `name` variable passed from the view.

## Using Filters

Filters are used to modify the output of variables in the Django Template Language. We can apply filters to variables by using the pipe `|` symbol followed by the filter name.

```html
<p>
    {{ value|filter }}
</p>
```

For example, we can use the `upper` filter to convert the value to uppercase.

```html
<p>
    {{ name|upper }}
</p>
```

## Using Tags

Tags are used to control the flow of the template and perform logic operations. Tags are enclosed in curly braces and percent signs `{% %}`.

```html
{% if condition %}
    <p>Condition is true</p>
{% else %}
    <p>Condition is false</p>
{% endif %}
```

In the above example, the content inside the `{% if %}` tag will be displayed if the condition is true, otherwise the content inside the `{% else %}` tag will be displayed.

## Using Loops

Loops are used to iterate over a list of items in the Django Template Language. We can use the `{% for %}` tag to loop over a list of items.

```html
<ul>
    {% for item in items %}
        <li>{{ item }}</li>
    {% endfor %}
</ul>
```

In the above example, the `{% for %}` tag will iterate over the `items` list and display each item in a list.

## Including Templates

We can include other templates in a template using the `{% include %}` tag. This allows us to reuse common template components across multiple templates.

```html
{% include 'header.html' %}
```

In the above example, the `header.html` template will be included in the current template.

## Extending Templates

We can extend a base template and override specific blocks in the child template using the `{% extends %}` and `{% block %}` tags.

Base template (`base.html`):

```html
<!DOCTYPE html>
<html>
<head>
    <title>{% block title %}My Site{% endblock %}</title>
</head>
<body>
    {% block content %}
    {% endblock %}
</body>
</html>
```

Child template (`child.html`):

```html
{% extends 'base.html' %}

{% block title %}Child Page{% endblock %}

{% block content %}
    <p>This is the content of the child page.</p>
{% endblock %}
```

In the above example, the `child.html` template extends the `base.html` template and overrides the `title` and `content` blocks.
