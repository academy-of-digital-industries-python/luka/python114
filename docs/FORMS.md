# Django Forms

Forms are an essential part of any web application. They allow users to interact with the application by entering data and submitting it to the server. In Django, forms are created using the `Form` class provided by the `django.forms` module.

## Creating a Form

To create a form in Django, we need to define a class that inherits from the `django.forms.Form` class. The class should define the fields of the form as class attributes. Each field is represented by an instance of a field class provided by Django.

```python
from django import forms

class MyForm(forms.Form):
    name = forms.CharField(label='Name', max_length=100)
    email = forms.EmailField(label='Email')
    message = forms.CharField(label='Message', widget=forms.Textarea)
```

In the above example, we have defined a form with three fields: `name`, `email`, and `message`. Each field is represented by an instance of a field class provided by Django. We can specify additional attributes for each field, such as the label and the maximum length.

## Rendering a Form in a Template

To render a form in a template, we need to create an instance of the form class in the view and pass it to the template context. We can then use the form instance in the template to render the form fields.

```html
<form method="post">
    {% csrf_token %}
    {{ form.as_p }}
    <button type="submit">Submit</button>

</form>
```

In the above example, we have rendered the form fields using the `{{ form.as_p }}` template tag. This tag renders the form fields as paragraphs. We have also included a CSRF token using the `{% csrf_token %}` template tag to protect against Cross-Site Request Forgery (CSRF) attacks.

## Validating Form Data

When a user submits a form, Django automatically validates the form data based on the field types and constraints defined in the form class. We can access the cleaned data using the `cleaned_data` attribute of the form instance.

```python
def my_view(request):
    if request.method == 'POST':
        form = MyForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            # Process the form data
    else:
        form = MyForm()
    return render(request, 'my_template.html', {'form': form})
```

In the above example, we have defined a view function that processes the form data. We create an instance of the form class with the POST data and check if the form is valid using the `is_valid()` method. If the form is valid, we can access the cleaned data using the `cleaned_data` attribute.

## Customizing Form Fields

We can customize the appearance and behavior of form fields by specifying additional attributes in the field definition. For example, we can specify the widget to use for a field, add custom validation logic, or define custom error messages.

```python
class MyForm(forms.Form):
    name = forms.CharField(label='Name', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email', error_messages={'invalid': 'Please enter a valid email address'})
    message = forms.CharField(label='Message', widget=forms.Textarea, validators=[validate_message])
```

In the above example, we have customized the appearance and behavior of the form fields by specifying additional attributes such as the widget, error messages, and validators.

