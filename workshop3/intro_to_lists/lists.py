""" This is a docstring
Data Structure - List

"""

ages = [25, 18, 19, 37, 45, 65]

print(ages)
# Access by index
print(ages[0])
print(ages[1])
print(ages[2])
print(ages[3])
# Error - Index out of range
# print(ages[105])

# change
ages[0] = 72
print(ages)
ages[0] += 1
print(ages)

# adding new element to list
ages.append(23)
print(ages)

ages.append(18)
print(ages)

ages.insert(3, 26)
print(ages)


# removing element form list
print(f'You removed {ages.pop()} from the list')
print(ages)
# remove from head
ages.pop(0)
print(ages)
# Error
# ages.pop(100)
# print(ages)

ages.remove(45)
print(ages)

# ValueError -> not found
# ages.remove(45)
# print(ages)

print(ages[len(ages) - 1])
print(ages[-1])
# Index Error
# print(ages[-100])
