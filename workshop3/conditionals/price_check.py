age = 17
price = 20

if age < 4:
    price = 0
elif age < 18:
    price = 25
elif age < 65:
    price = 40

# Wrong sequence of ifs
# if age < 65:
#     price = 40
# elif age < 18:
#     price = 25
# elif age < 4:
#     price = 0

# Correct but bad
# if 18 <= age < 65:
#     price = 40
# elif 4 <= age < 18:
#     price = 25
# elif age < 4:
#     price = 0


print(f"Your admission cost is ${price}.")