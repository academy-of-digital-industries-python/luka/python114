password = 'Ag4534fwesfwe3'
error_messages = ''
# დაწერეთ შესაბამისი ერორები
# შეამოწმეთ პაროლის სიგრზე 8 -ზე მეტი უნდა იყოს len()
# შეამოწმეთ მხოლოდ მაღალ რეგისტრში ხომ არ არის
# შეამოწმეთ მხოლოდ დაბალ რეგისტრში ხომ არ არის

if len(password) < 8:
    # error_messages = error_messages + '\t * Password must be longer than 8 characters!'
    error_messages += '\t * Password must be longer than 8 characters!\n'
if password.islower():
    error_messages += '\t * Password should contain at least 1 upper case character\n'
if password.isupper():
    error_messages += '\t * Password should contain at least 1 lower case character\n'

# if error_messages != '':
if error_messages:
    print('Please Check your password:')
    print(error_messages)
