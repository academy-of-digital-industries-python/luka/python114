is_raining = False
temperature = 25
# 25 -> cool
# 23 -> nothing
# 19 -> heat

if is_raining:
    # Code block
    print('initiate raining protocol')
    print('send alert message to ... this number')
    print('send alert message to ... this email address')
    print('close the roof')
    # end block

# თუ ტემპერატურა მეტა 24 -ზე ჩაირთოს გაგრილება, გამოიგზავნოს მესიჯი
# თუ ტემპერატურა ნაკლებია 20 -ზე ჩაირთოს გათბობა, გამოიგზავნოს მესიჯი
if temperature > 24:
    print('Turn on AC')
    print('send alert message to ... this number (AC)')
elif temperature < 20 and temperature > 12:
    print('Turn on Heating')
    print('send alert message to ... this number (Heating)')
elif temperature > -5 and temperature > 12:
    print('Shut down facility')
    print('send alert message to ... this number (ERROR)')

