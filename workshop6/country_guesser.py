"""
we have a country with their capitals and user should guess.
countries should be chosen by random and dispaly it to user
"""
import random


def get_user_input(country):
    return input(f'What is the capital of {country}? ').lower()

def play():
    countries = [
        ('France', 'Paris'),
        ('Germany', 'Berlin'),
        ('Georgia', 'Tbilisi')
    ]
    while countries:
        # country, capital = countries[random.randint(0, len(countries) - 1)]
        coutry_element = random.choice(countries)
        country, capital = coutry_element
        guess = get_user_input(country)
        if guess == capital.lower():
            print('You are correct!')
        else:
            print(f'You are not correct, {country} capital is {capital}')

        countries.remove(coutry_element)

    print('Game is over!')

def main():
    while input('Do you want to play? (y/n)').lower() == 'y':
        play()

main()
