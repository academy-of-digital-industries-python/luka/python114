def add(a = 0, b = 0):
    return a + b

def subtract(a, b):
    return a - b


# positional
print(f'{add(5, 7) = }')
print(f'{add(7, 5) = }')
print(f'{add(7) = }')
print(f'{add() = }')



print(f'{subtract(5, 7) = }')
print(f'{subtract(7, 5) = }')


print(
    add(5, add(add(9, 9), add(9, 0)))
)
