# global Scope
g = 9.8

def greet_user(username):  # definition of function
    # Block of code; code block; code fragment
    # Local scope
    greeting_text = 'Hi'

    full_text = f'{greeting_text} {username}'
    print(g)
    return full_text


# print(greet_user)
# greet_user()  # call; invoke
# greet_user()
# greet_user()
# greet_user()
print(greet_user('Josh'))
print(greet_user('Jane'))
print(greet_user('James'))
print(greet_user('Jimmy'))
print(f'From global view: {g}')

