# How to Setup python project

### Pre-requisites
- Virtualenv `pip install virtualenv`
- Or some other virtual environment manager like `conda`, `pipenv`, `poetry` etc.

### How to create new project 
- Create a new directory for your project
- Create a virtual environment  `virtualenv venv`
- Activate the virtual environment `source venv/bin/activate` **or** `venv\Scripts\activate`
- Add dependencies `pip install <package_name>`
- Create a requirements.txt file `pip freeze > requirements.txt`
- initialize git `git init`
- Create a **.gitignore** file

### How to setup a existing python project
- Clone the project (if it is a git repository)
- Create a virtual environment  `virtualenv venv`
- Activate the virtual environment `source venv/bin/activate` **or** `venv\Scripts\activate`
- Install dependencies `pip install -r requirements.txt`
- Run the project (depends on the project)


### Example Project Structure
```bash
project_name/
│
├── venv/
├── requirements.txt
├── .gitignore
├── README.md
├── src/
│   ├── __init__.py
│   ├── main.py
│   ├── module1.py
│   ├── module2.py
├── tests/
│   ├── __init__.py
│   ├── test_module1.py
│   ├── test_module2.py
```


