import datetime

username = input('What is your name? ')

print(f'Hello {username}')

data_file = open('./dat.txt', 'a', encoding='UTF-8')

now = datetime.datetime.now()
print(now.strftime('%A %d/%B/%Y %H:%M:%S'))

# be careful with quotes with f string
data_file.write(f'{username} used program at {now.strftime('%A %d/%B/%Y %H:%M:%S')}\n')

data_file.close()
