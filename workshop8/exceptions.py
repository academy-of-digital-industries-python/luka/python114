"""
Error handling
"""

try:
    number = int(input('Choose a number: '))
except ValueError:
    print('Please type only number! ')
else:
    try:
        print(number / number)
    except ZeroDivisionError:
        print('Please enter number that is greater that 0')
finally:
    print('I will be executed no matter what!')
