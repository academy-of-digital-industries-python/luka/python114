
try:
    number = int(input('Choose a number: '))
    print(number / number)
# except (ValueError, ZeroDivisionError):
#     print('Please type only number! ')
except ValueError:
    print('Please type only number! ')
except ZeroDivisionError:
    print('Number must be greater than 0')
except Exception as e:
    print(e)
    print('Something unhandled happened!')
    
