import random
def game():
    lower_bound = 1
    upper_bound = 100
    number = random.randint(lower_bound, upper_bound)
    guesses = 0

    while True:
        guess = int(input(f'guess a number from {lower_bound} to {upper_bound} '))
        guesses += 1

        if guess < number:
            print('Wrong number! Try higher one')
        elif guess > number:
            print('Wrong number! Try lower one')
        else:
            print('Correct!')
            break

game()