from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

# print(BASE_DIR, type(BASE_DIR))

# RAW string
with open(BASE_DIR / 'dat.txt', encoding='UTF-8') as f:
    print(f.read())
