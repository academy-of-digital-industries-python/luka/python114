
def f(x, z):
    return x ** z

# positional
print(f(2, 5))
print(f(5, 2))

# keyword arguments
# below function calls are equivalent
print(f(x = 2, z = 5))
print(f(z = 5, x = 2))
print(f(2, z = 5))


