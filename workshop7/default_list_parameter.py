def append(element, arr = None):
    if arr is None:
        arr = []
    # local arr parameter will shadow global arr variable
    print(f'arr {id(arr)} from local: {arr}')
    arr.append(element)


arr = []

append(5)
append(5)
append(5, arr)
append(5)

print(arr)