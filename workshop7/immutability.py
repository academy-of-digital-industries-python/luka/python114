first_name = 'hello'  # immutable
nums = (1, 2, 3, 4)   # immuatble
nums2 = [1, 2, 3, 4]  # mutable

# Immutable
# first_name[0] = 'k'
print(first_name[0])


# nums[0] = 8
print(nums)


nums2[0] = 8
print(nums2)
