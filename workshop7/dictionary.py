"""
Data Structure: Dictionary (HashMap, Map) - dict

dictionary constits key value pairs

{
    key: value
}
"""


student = {
    'first_name': 'John',
    'last_name': 'Doe',
    'age': 45
}
student2 = {
    'first_name': 'Jane',
    'last_name': 'Doe',
    'age': 45
}

print(student)
print(student2)

print(student['age'] is student2['age'])
print(id(student['age']), id(student2['age']))

student['first_name'] = 'Josh'
print(student)

student['pet'] = {
    'type': 'dog',
    'name': 'Alice',
    'age': 3,
    'favorite_toys': ['ball', 'bone']
}
print(student)

print(student['pet']['favorite_toys'][0])

students = [
    {
        'first_name': 'John',
        'last_name': 'Doe',
        'age': 23,
        'friends': ['Jonnah', 'George'],
        'hobbies': ['Dancing']
    },
    {
        'first_name': 'Jane',
        'last_name': 'Doe',
        'age': 21,
        'friends': ['jimmy']
    },
]


for student in students:
    if not student['friends']:
        continue
    print(student.get('hobbies'))
    print(student['friends'][0])
    student.pop('age')

print(students)