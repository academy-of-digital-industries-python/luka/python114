person = {
    'name': 'Josh',
    'last_name': 'Doe',
    'age': 35
}


# Keys
print('\nKeys:')
for key in person.keys():
    # print(key, person[key])
    print(key)

print('\nValues:')
for value in person.values():
    print(value)


print('\nItem:')
for key, value in person.items():
    print(key, value)
