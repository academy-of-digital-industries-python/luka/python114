import sys

names = ['josh'] * 100

print(len(names))
print(sys.getsizeof(names))
print(sys.getsizeof('josh'))
