import json
import time
import datetime
from pathlib import Path
from generator import generate_and_save
from find import find_and_display_row
from utils import load_data_file

BASE_DIR = Path(__file__).resolve().parent
DATA_PATH_JSON = BASE_DIR / 'data' / 'ETH.json'
DATA_PATH_CSV = BASE_DIR / 'data' / 'ETH.csv'


def main():
    while True:
        command = input('Command: ').lower()
        if command == 'generate':
            try:
                record_count = int(input('Enter Recrod count: '))
            except ValueError:
                print('Please enter number correctly')
                continue

            print('Generating Data...')
            generate_and_save(DATA_PATH_CSV, DATA_PATH_JSON, record_count)
            print('Data Successfully generated')
        elif command == 'find':
            find_and_display_row(DATA_PATH_JSON)
        elif command == 'analyze':
            data = load_data_file(DATA_PATH_JSON)

            max_price = None
            min_price = None
            for timestamp, row in data.items():
                if max_price is None or max_price['price'] < row['price']:
                    max_price = {
                        'datetime': datetime.datetime.fromtimestamp(int(timestamp)),
                        **row
                    }
                if min_price is None or min_price['price'] > row['price']:
                    min_price = {
                        'datetime': datetime.datetime.fromtimestamp(int(timestamp)),
                        **row
                    }

            print(f'Min Pirce was at: {min_price["datetime"]} @ {min_price["price"]}')
            print(f'Min Pirce was at: {max_price["datetime"]} @ {max_price["price"]}')
        elif command == 'stream':
            while True:
                data = load_data_file(DATA_PATH_JSON)
                for timestamp, row in data.items():
                    print(f'{datetime.datetime.fromtimestamp(int(timestamp))} | {row['price']}')
                    time.sleep(0.25)
        elif command in ['quit', 'exit']:
            print('Thank you!')
            break
        else:
            print('Unknown Command')


if __name__ == '__main__':
    main()
