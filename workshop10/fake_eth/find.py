import json
import datetime
import sys
from utils import load_data_file

def find_and_display_row(data_path):
    timestamp = input('Enter timestamp: ')
    data = load_data_file(data_path)
    print(f'data variable has consumed {sys.getsizeof(data)} bytes')
    row = data.get(timestamp)
    if row is None:
        print('There is no such timestamp in this data')
        return
    print(f'Price at {datetime.datetime.fromtimestamp(int(timestamp))} was {row["price"]}')