import json

def load_data_file(data_path):
    with open(data_path, encoding='utf-8') as f:
        return json.load(f)
