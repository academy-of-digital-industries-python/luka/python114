"""
Generate Fake Etherium data
"""
import random
import datetime
import csv
import json


def generate_fake_data(price = 2000, date_ = None, record_count = 10_000):
    if date_ is None:
        date_ = datetime.datetime.now().replace(microsecond=0)

    data = []
    for i in range(1, record_count + 1):
        data.append({
            'id': i,
            'price': price,
            'timestamp': date_
        })
        date_ += datetime.timedelta(seconds=1)
        price += random.randint(-20, 20)

    return data


def generate_and_save(data_path_csv, data_path_json, record_count = 10_000):
    data = generate_fake_data(record_count=record_count)
    with open(data_path_csv, 'w', encoding='UTF-8') as f:
        writer = csv.writer(f)
        writer.writerow(['id', 'price', 'timestamp'])
        writer.writerows([
            (row['id'], row['price'], row['timestamp'])
            for row in data
        ])

    with open(data_path_json, 'w', encoding='UTF-8') as f:
        json_data = {
            int(row['timestamp'].timestamp()): {
                'id': row['id'],
                'price': row['price']
            } for row in data
        }
        json.dump(json_data, f, indent=4)
