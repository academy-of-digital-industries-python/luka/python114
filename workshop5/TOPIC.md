# Workshop 5 topics
- user input
- enumeration - function
- list comprehension
- slicing
- introduction to new data structure - tuple
- matrices - 2d list

## Exercise - 2d cleaner robot
We have a 2d list with numbers inside that represent room with obstacles and cleaner robot. 

The robot can move up, down, left and right. 

The robot can't move through obstacles. 

The robot can clean the room by changing the number to 0. 

The robot can't move outside the room. 

The robot can't move through the wall and the roobot cannot teleport!
representaion table

| number | representation |
| ------ | -------------- |
| 0      | unclean sapce  |
| 1      | obstacle       |
| 2      | robot          |
| 3      | clean space    |



## Challenge
- add moving obstacles to cleaner robot
- Guessing game