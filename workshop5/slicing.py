import random

numbers = [
    random.randint(-100, 100)
    for _ in range(10)
]

print(numbers)
print(f'{numbers[0:5] = }') # [start:stop:step]
print(f'{numbers[:5] = }')
print(f'{numbers[5:10] = }')
print(f'{numbers[5:] = }')
print(f'{numbers[::2] = }')
print(f'{numbers[:] = }')
print(f'{numbers[:] == numbers = }')
print(f'{numbers[:] is numbers = }')
