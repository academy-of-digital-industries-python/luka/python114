import os
import platform

room = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 2, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1]
]

y = 2
x = 1
# print(room[y][x])

# ask user for movement indefinetly

while True:
    # if platform.system() == 'Windows':
    #     os.system('cls')
    # else:
    #     os.system('clear')

    os.system('cls' if platform.system() == 'Windows' else 'clear')
    # drawing
    for row in room:
        for cell in row:
            if cell == 0:
                print('🍌', end='')
            elif cell == 1:
                print('🧱', end='')
            elif cell == 2:
                print('🧹', end='')
            else:
                print('⬛', end='')
        print()

    # Getting input
    direction = input('a (left) / w (up) / s (down) / d (right): ').lower()

    # cheking directions
    if direction == 'a':
        dx = -1
        dy = 0
    elif direction == 'w':
        dx = 0
        dy = -1
    elif direction == 's':
        dx = 0
        dy = 1
    elif direction == 'd':
        dx = 1
        dy = 0
    else:
        print('Wrong direction')
        continue

    if room[y + dy][x + dx] == 1:
        print('You Cannot move there!')
        continue

    # handling movement
    room[y][x] = 3
    room[y + dy][x + dx] = 2
    x += dx
    y += dy
