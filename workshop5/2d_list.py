import random
# grid = [
#     [0, 0, 1, 1, 1, 1],
#     [1, 0, 0, 0, 1, 1],
#     [1, 0, 0, 0, 1, 1],
#     [1, 0, 0, 0, 1, 1],
#     [1, 0, 0, 0, 1, 1],
#     [1, 0, 0, 0, 8, 1],
#     [1, 1, 1, 1, 1, 1],
# ]

grid = [
    [
        random.randint(0, 1)
        for _ in range(10)
    ]
    for _ in range(10)
]

print(grid[0][0])
print(grid[-2][-2])

for row in grid:
    for cell in row:
        if cell == 1:
            print('#', end='')
        elif cell == 8:
            print('*', end='')
        else:
            print(' ', end='')
    print()
