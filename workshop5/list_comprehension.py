import random

# მჭირდება ლისტი სადაც იქნება 1000 ცალი რენდომ რიცხვი!
# როგორ მივიღო რენდომ რიცხვი პითონში?       - random.randint(0, 100)
# როგორ გავამეორო ეს ოპერაცია 1000 ჯერ?       - ციკლი
# როგორ ჩავამატო ლისტში მნიშვნელობა?         - .append method

# numbers = []

# for i in range(1000):
#     numbers.append(random.randint(-100, 100))


# numbers = [number for number in range(1000)]
numbers = [
    random.randint(-100, 100)
    for _ in range(1000)
]


numbers = [
    number  # what to append
    for number in numbers  # from where to take this element
    if number % 2 == 0  # condition
]

print(numbers)
print(len(numbers))
