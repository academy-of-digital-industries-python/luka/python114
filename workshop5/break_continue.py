import random
import time

while True:
    number = random.randint(0, 100)
    if number > 90:
        # skip the iteration
        continue

    if 40 < number < 60:
        # stop the loop
        break

    print(number)
    time.sleep(1)
