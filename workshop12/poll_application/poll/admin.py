from django.contrib import admin
from poll.models import Question, Option

admin.site.register([Question, Option])
