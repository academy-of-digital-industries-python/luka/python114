from django import forms
from poll.models import Question


class QuestionCreateForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text']
