# Object oriented programming
## Classes
### Introduction
- Python is an object oriented programming language.
- Almost everything in Python is an object, with its properties and methods.
- A Class is like an object constructor, or a "blueprint" for creating objects.

### Create a Class
- To create a class, use the keyword class:
```python
class MyClass:
  x = 5
```

### Create Object
- Now we can use the class named MyClass to create objects:
```python
p1 = MyClass()
print(p1.x)
```

### The `__init__(self)` Function

- The examples above are classes and objects in their simplest form, and are not really useful in real life applications.
- To understand the meaning of classes we have to understand the built-in `__init__()` function.
- All classes have a function called `__init__()`, which is always executed when the class is being initiated.
- Use the `__init__()` function to assign values to object properties, or other operations that are necessary to do when the object is being created:
```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

p1 = Person("John", 36)

print(p1.name)
print(p1.age)
```

### Object Methods
- Objects can also contain methods. Methods in objects are functions that belong to the object.
- Let us create a method in the Person class:
```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)

p1 = Person("John", 36)
p1.myfunc()
```

### The self Parameter
- The self parameter is a reference to the current instance of the class, and is used to access variables that belongs to the class.
- It does not have to be named self , you can call it whatever you like, but it has to be the first parameter of any function in the class:
```python

class Person:
  def __init__(mysillyobject, name, age):
    mysillyobject.name = name
    mysillyobject.age = age

  def myfunc(abc):
    print("Hello my name is " + abc.name)

p1 = Person("John", 36)
p1.myfunc()
```

### Modify Object Properties
- You can modify properties on objects like this:
```python
p1.age = 40
```

### Inheritance
- Inheritance allows us to define a class that inherits all the methods and properties from another class.
- Parent class is the class being inherited from, also called base class.
- Child class is the class that inherits from another class, also called derived class.
```python
class Student(Person):
  pass
```

### Add the `__init__(self)` Function

- When you add the `__init__()` function, the child class will no longer inherit the parent's` __init__()` function.
- The child's `__init__()` function overrides the inheritance of the parent's `__init__()` function.
- To keep the inheritance of the parent's `__init__()` function, add a call to the parent's `__init__()` function:
```python
class Student(Person):
  def __init__(self, fname, lname):
    Person.__init__(self, fname, lname)
```

### Use the super() Function
- Python also has a super() function that will make the child class inherit all the methods and properties from its parent:
```python
class Student(Person):
  def __init__(self, fname, lname):
    super().__init__(fname, lname)
```

### Add Properties
- Add a property called graduationyear to the Student class:
```python
class Student(Person):
  def __init__(self, fname, lname):
    super().__init__(fname, lname)
    self.graduationyear = 2019
```

### Add Methods
- Add a method called welcome to the Student class:
```python

class Student(Person):
  def __init__(self, fname, lname):
    super().__init__(fname, lname)
    self.graduationyear = 2019

  def welcome(self):
    print("Welcome", self.firstname, self.lastname, "to the class of", self.graduationyear)
```

### Classwork
- Create a class called `Car` with the following properties:
  - `brand`
  - `model`
  - `year`
- Create a method called `start` that prints "The car is starting"
- Create an object of the class `Car` and call the `start` method
- Create a class called `ElectricCar` that inherits from the `Car` class
- Add a property called `battery` to the `ElectricCar` class
- Create an object of the class `ElectricCar` and call the `start` method
- Create a method called `charge` that prints "The car is charging"
- Create an object of the class `ElectricCar` and call the `charge` method
- Create a class called `FossilFuelCar` that inherits from the `Car` class
- Add a property called `fuel` to the `FossilFuelCar` class
- Create an object of the class `FossilFuelCar` and call the `start` method
- Create a method called `refuel` that prints "The car is refueling"
- Create an object of the class `FossilFuelCar` and call the `refuel` method
- Create a class called `HybridCar` that inherits from the `ElectricCar` and `FossilFuelCar` classes
- Create an object of the class `HybridCar` and call the `start` method
- Create an object of the class `HybridCar` and call the `charge` method
- Create an object of the class `HybridCar` and call the `refuel` method
- Create a method called `drive` that prints "The car is driving"
- Create an object of the class `HybridCar` and call the `drive` method
- Create a method called `park` that prints "The car is parking"
- Create an object of the class `HybridCar` and call the `park` method
