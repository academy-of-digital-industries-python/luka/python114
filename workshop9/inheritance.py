from classes import Person
import random

class Student(Person):
    def __init__(self, name, age, gpa = 4.0):
        super().__init__(name, age)
        self.gpa = gpa

    def show_gpa(self):
        print(f'{self.name} has {self.gpa}')


class Teacher(Person):
    pass

student = Student('Luka', 25)


student.greet()
student.show_gpa()


students = [
    Student(random.choice(['a', 'b', 'c']), random.choice(range(25, 50)))
    for _ in range(10)
]


for student in students:
    print(student)
    student.greet()

