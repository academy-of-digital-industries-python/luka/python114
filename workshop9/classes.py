class Person:
    def __init__(self, name, age):
        # constructor
        self.name = name
        self.age = age

    def greet(self):
        print(f'Hello, my name is {self.name} and I am {self.age} years old')


if __name__ == '__main__':
    p1 = Person('Josh', 57)  # instatiation / object creation
    p2 = Person('Jane', 35)

    p1.greet()
    p2.greet()
    # print(p1.name, p1.age)
    # print(p2.name, p2.age)
