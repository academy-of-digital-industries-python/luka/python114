import random
from pprint import pprint
import time
# x, y; x < y;
# we have to find z
numbers = sorted(
    list(
        set([
            random.randint(0, 10 ** 6)
            for _ in range(10 ** 6)
        ])
    )
)

def find(z, list_):
    # linear search / brute force
    for i, x in enumerate(list_):
        if x == z:
            return i
    return -1


def binary_search(z, list_):
    lower_bound = 0
    upper_bound = len(list_) - 1
    steps = 0
    while lower_bound <= upper_bound:
        middle = (lower_bound + upper_bound) // 2
        num = list_[middle]
        steps += 1
        if num > z:
            upper_bound = middle - 1
        elif num < z:
            lower_bound = middle + 1
        else:
            print(f'Took {steps} steps')
            return middle
    print(f'Took {steps} steps')
    return None


# s = time.time()
# print(find(7, numbers))
# print(time.time() - s)

# s = time.time()
# print(find(960_001, numbers))
# print(time.time() - s)
print(len(numbers))
print(binary_search(6_000_000, numbers))
print(f'Find {numbers[-1]}')
found_index = binary_search(numbers[-1], numbers)
print(f'Index is {found_index}; value: {numbers[found_index]}')

print(f'Find {numbers[0]}')
found_index = binary_search(numbers[0], numbers)
print(f'Index is {found_index}; value: {numbers[found_index]}')
